USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[RP_GET_USER_REPORTS]    Script Date: 9/1/2018 12:58:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




create   PROCEDURE [dbo].[RP_GET_USER_REPORTS](
	@userid_str varchar(MAX) = null,
	@report_id_str varchar(max) = null,
	@start_dt datetime = null,
	@end_dt datetime = null, 
	@mode_str varchar(4000)=null
)

AS

SET NOCOUNT ON

/*************************************************************************************************************
New Procedure CWH 8/28/2018 this report lists report requests. 

exec RP_GET_USER_REPORTS @userid_str = 'chall' ,@start_dt = '08/22/2018', @report_id_str = null
exec ap_get_parameter_values 7475
grant execute on RP_GET_USER_REPORTS to impusers
**************************************************************************************************************/

declare @users table (userid char(8), lname varchar(30), fname varchar(30))

IF @userid_str is null or ISNULL(Datalength(ltrim(@userid_str)),0) = 0
	insert @users
	select userid, 
			lname, 
			fname 
	from T_METUSER 
else 
	insert @users
	select userid, 
			lname, 
			fname
	from T_METUSER a 
	join dbo.FT_SPLIT_LIST(@userid_str,',') e on e.Element=a.userid

Declare @reports table (id varchar(100))

IF @report_id_str is null or ISNULL(Datalength(ltrim(@report_id_str)),0) = 0
	insert @reports 
	select id 
	from gooesoft_report
ELSE
	insert @reports
	select id 
	from gooesoft_report a
	join dbo.FT_SPLIT_LIST(@report_id_str,',') e on e.Element=a.id



select a.id as request_id,
		a.report_id, 
		b.name, 
		a.[user_id], 
		u.fname,
		u.lname,
		a.ug_id,
		b.report_type,
		case a.type 
		when 'f' then 'User Run' 
		when 'b' then 'User Run'
		when 'h' then 'Schedule Setup'
		when 's' then 'Scheduled'
		end as Mode,
		a.request_date_time, 
		a.end_date_time, 
		rt.Description as ReportType,
		rc.description as ReportCat,
		b.psr_file,
		
		a.result_code,
		a.printer_name,
		a.email_recipients, 
		a.email_subject, 
		a.email_body
		

from gooesoft_request a 
join gooesoft_report b on a.report_id = b.id
join @reports r on b.id=r.id 
join TR_REPORT_TYPE rt on rt.Id=b.report_type
join @users u on u.userid = a.[user_id]
join  gooesoft_report_category rc on rc.id=b.category

where (@start_dt is null or a.request_date_time >= @start_dt)
and (@end_dt is null or a.request_date_time <= @end_dt)
and a.type = 'f'
order by a.request_date_time desc 


